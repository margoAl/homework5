import java.util.*;

public class TreeNode {

	private String name;
	private TreeNode firstChild;
	private TreeNode nextSibling;

	/*
	 * Kasutan ideid:
	 * https://www.cs.duke.edu/courses/cps100/spring13/Code/Rec8/BST.java
	 * http://interactivepython.org/runestone/static/pythonds/Trees/ParseTree.
	 * html https://en.wikipedia.org/wiki/Parse_tree
	 */

	public TreeNode(String n, TreeNode d, TreeNode r) {
		name = n;
		firstChild = d;
		nextSibling = r;
	}

	
	public static TreeNode parsePrefix(String s) {
		if (s.isEmpty() || s == null)
			throw new RuntimeException(s + " empty string!");
		if (!s.contains("(") && s.contains(",") && !s.contains(")"))
			throw new RuntimeException(s + " no brackets!");
		
//		 if (s.contains("),")) {
//		 throw new RuntimeException(s + " contains comma and bracket.");
//		 }
		return parsePrefix2(s);
	}

	public static TreeNode parsePrefix2(String s) {

		// 1. Kontrollin kas string on tühi vői mitte
		if (s.isEmpty() || s == null) {
			return null;
		}

		if (s.contains("()")) {
			throw new RuntimeException(s + " contains empty subtree.");
		}
		if (s.contains("((") && s.contains("))")) {
			throw new RuntimeException(s + " contains double brackets.");
		}
		if (s.contains(",,")) {
			throw new RuntimeException(s + " contains double commas.");
		}
		if (s.contains(" ")) {
			throw new RuntimeException(s + " contains empty spaces.");
		}
		if (s.contains("(,") || s.contains(",)")) {
			throw new RuntimeException(s + " contains comma and bracket.");
		}
		if (s.trim().isEmpty()) {
			throw new RuntimeException(s + " is empty tab!");
		}

		int left = 0, right = 0;

		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == '(') {
				left++;
			} else if (s.charAt(i) == ')') {
				right++;
			}
		}

		if (left != right) {
			throw new RuntimeException(s + " is unbalanced.");
		}

	

		// 2. Loon tokenizatori stringi lahti harutamiseks
		// Kasutan naidet ja ideed -
		// http://www0.cs.ucl.ac.uk/staff/ucacwxe/lectures/Z23-04-05/poa/sample/bank/DBUtils.java

		else

		{
			return parseString(s);

		}

	}

	private static TreeNode parseString(String input) {

		StringTokenizer stoken = new StringTokenizer(input, "(,) ", true);
		String stn = stoken.nextToken();
		String first = "";
		String second = "";
		String third = "";

		while (stoken.hasMoreTokens()) {
			first = stoken.nextToken();
			if (first.equals("(")) {
				first = stoken.nextToken();
				// System.out.println(first);
				int closed = 0;
				while (closed < 1) {
					second = second + first;
					first = stoken.nextToken();
					if (first.equals("(")) {
						closed = closed - 1;
					} else if (first.equals(")")) {
						closed = closed + 1;
					}
					// System.out.println(first);
					// System.out.println(second);

				}
			}
			if (first.equals(",")) {
				while (stoken.hasMoreTokens()) {
					third = third + stoken.nextToken();
					// System.out.println(third);

				}
			}

		}

		return new TreeNode(stn, TreeNode.parsePrefix2(second), TreeNode.parsePrefix2(third));

	}

	public String rightParentheticRepresentation() {

		// TODO!!! create the result in buffer b
		StringBuffer b = new StringBuffer();
		TreeNode TreeNode = firstChild;
		// 1. Kui firstchild ei ole null, siis lisan ( ja hakka puud kokku
		// panema
		if (firstChild != null)
			b.append('(');
		// 2. Kuniks TreeNode ei ole null, lisan liikmeid
		while (TreeNode != null) {
			b.append(TreeNode.rightParentheticRepresentation());
			TreeNode = TreeNode.nextSibling;
			// 3. Lisan eraldaja puuliikmete vahele.
			if (TreeNode != null)
				b.append(',');
		}
		// Lisan sulu
		if (firstChild != null)
			b.append(')');

		return b.append(name).toString();

	}

	public static void main(String[] param) {
		// String s = "";
		// String s = "ABC";
		// String s = "A(,B)";
		// String s = "A(B1,C,D)";
		// String s = "A(B,C),D";
		 String s = "A(B),C(D)";
		// String s = "A(,B)";
		// String s = "A,B";
//		String s = "+(*(-(2,1),4),/(6,3))";
		TreeNode t = TreeNode.parsePrefix(s);
		String v = t.rightParentheticRepresentation();
		System.out.println(s + " ==> " + v); // A(B1,C,D) ==> (B1,C,D)A
	}
}